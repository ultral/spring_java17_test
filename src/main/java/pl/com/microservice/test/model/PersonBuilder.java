package pl.com.microservice.test.model;

import java.math.BigDecimal;
import java.util.Date;

public final class PersonBuilder {
    private String name;
    private Date bithDate;
    private BigDecimal salary;
    private int id;
    private PositionEnum position;

    private PersonBuilder() {
    }

    public static PersonBuilder aPerson() {
        return new PersonBuilder();
    }

    public PersonBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder withBithDate(Date bithDate) {
        this.bithDate = bithDate;
        return this;
    }

    public PersonBuilder withSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    public PersonBuilder withId(int id) {
        this.id = id;
        return this;
    }

    public PersonBuilder withPosition(PositionEnum position) {
        this.position = position;
        return this;
    }

    public PersonBuilder likePerson(Person person) {
        this.withPosition(person.getPosition()).withSalary(person.getSalary()).withId(person.getId()).withName(person.getName()).withBithDate(person.getBirthDate());
        return this;
    }

    public Person build() {
        return new Person(name, bithDate, salary, id, position);
    }
}
