package pl.com.microservice.test.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static pl.com.microservice.test.model.PositionEnum.*;

@Component
public class Initializer {

    private final Environment environment;

    @Autowired
    public Initializer(final Environment environment, final PersonRepository personRepository) {
        this.environment = environment;
        //todo can it be done better?
        initializePersonRepository(personRepository);
    }

    public static List<Person> initPersonList() {
        return List.of(
                PersonBuilder.aPerson().withName("Marek").withBithDate(new Date(1_220_227_200L)).withSalary(ONE).withPosition(worker).build(),
                PersonBuilder.aPerson().withName("Czarek").withBithDate(new Date(1_320_227_200L )).withSalary(TEN).withPosition(manager).build(),
                PersonBuilder.aPerson().withName("Czarkosław").withBithDate(new Date(1_420_227_200L )).withSalary(TEN.multiply(TEN)).withPosition(president).build()
        );
    }

    private void initializePersonRepository(final PersonRepository personRepository) {
        if (Arrays.asList(environment.getActiveProfiles()).contains("dev")) {
            initPersonList().forEach(personRepository::save);
        }
    }
}
