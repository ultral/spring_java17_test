package pl.com.microservice.test.model;

public enum PositionEnum {
    president,
    manager,
    worker,
    unknown
}
