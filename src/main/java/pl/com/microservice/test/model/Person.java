package pl.com.microservice.test.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "persons")
public class Person {

    private static final BigDecimal ZERO = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private BigDecimal salary;
    private PositionEnum position;

    public Person() {}

    public Person(final String name, final Date birthDate, final BigDecimal salary, final int id, final PositionEnum position) {
        this.setName(name);
        this.setBirthDate(birthDate);
        this.setSalary(salary);
        this.setId(id);
        this.setPosition(position);
    }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public int getId() {
        return id;
    }

    public PositionEnum getPosition() {
        return position;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = (salary != null ? salary.setScale(2, RoundingMode.HALF_UP) : ZERO);
    }

    public void setPosition(PositionEnum position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) && Objects.equals(birthDate, person.birthDate) && position == person.position &&
                ((salary != null && salary.compareTo(person.salary) == 0) || (person.salary == null));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthDate, Objects.requireNonNullElse(salary, ZERO).toPlainString(), position);
    }
}
