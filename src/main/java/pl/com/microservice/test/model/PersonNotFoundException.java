package pl.com.microservice.test.model;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "person not found")
public class PersonNotFoundException extends RuntimeException {
    private final int id;

    public PersonNotFoundException(final int id) {
        this.id = id;
    }

    @Override
    public String getMessage() {
        return "Person with id: '" + id + "' not found";
    }
}