package pl.com.microservice.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.com.microservice.test.model.Person;
import pl.com.microservice.test.model.PersonNotFoundException;
import pl.com.microservice.test.model.PersonRepository;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

@SuppressWarnings("unused")
@RestController
public class PersonRestController {

    private final PersonRepository personRepository;

    @Autowired
    public PersonRestController(final PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("/persons")
    public List<Person> getAll() {
        final List<Person> persons = new ArrayList<>();
        personRepository.findAll().forEach(persons::add);
        return persons;
    }

    @GetMapping("/persons/name/{name}")
    public List<Person> getByName(@PathVariable final String name) {
        if (name == null) {
            return emptyList();
        }
        final List<Person> persons = new ArrayList<>();
        personRepository.findAll().forEach(it -> {
            if (name.equals(it.getName())) {
                persons.add(it);
            }
        });
        return persons;
    }

    @GetMapping("/persons/id/{id}")
    public Person getById(@PathVariable final int id) {
        return personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException(id));
    }

    @DeleteMapping("/persons/id/{id}")
    public void deleteById(@PathVariable final int id) {
        personRepository.deleteById(id);
    }

    /* example json post to: http://localhost:8080/persons
      {
        "name": "Marek",
        "birthDate": "2021-09-24T10:59:37.171+00:00",
        "salary": 1,
        "id": 4,
        "position": "worker"
      }
     */
    @PostMapping("/persons")
    public void post(@RequestBody final Person newPerson) {
        personRepository.save(newPerson);
    }

    /* example json put to: http://localhost:8080/persons/4
      {
        "name": "Marek",
        "birthDate": "2021-09-24T10:59:37.171+00:00",
        "salary": 1,
        "id": 4,
        "position": "worker"
      }
     */
    @PutMapping("/persons/{id}")
    public void put(@RequestBody final Person newPerson, @PathVariable final int id) {
        var person = personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException(id));
        person.setName(newPerson.getName());
        person.setSalary(newPerson.getSalary());
        person.setPosition(newPerson.getPosition());
        person.setBirthDate(newPerson.getBirthDate());
        personRepository.save(person);
    }
}
