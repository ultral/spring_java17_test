package pl.com.microservice.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("unused")
@RestController
public class LoggingErrorController implements ErrorController {
    static final String DEFAULT_ERROR_ATTRIBUTE_NAME = DefaultErrorAttributes.class.getName() + ".ERROR";
    static final String SERVLET_ERROR_ATTRIBUTE_NAME = "javax.servlet.error.exception";
    static final String SERVLET_URI_ATTRIBUTE_NAME = "javax.servlet.error.request_uri";
    static final String SERVLET_STATUS_CODE_ATTRIBUTE_NAME = "javax.servlet.error.status_code";
    static final Logger logger = LoggerFactory.getLogger(LoggingErrorController.class);

    @RequestMapping(value = "/error")
    public String error(final HttpServletRequest request) {
        final Throwable errorCause = getErrorCause(request);
        final String uri = (String) request.getAttribute(SERVLET_URI_ATTRIBUTE_NAME);
        if (errorCause != null) {
            logger.error("uri: " + uri + " error: " + errorCause.getMessage(), errorCause);
            return buildResponse(errorCause);
        }
        final HttpStatus status = getStatus((Integer) request.getAttribute(SERVLET_STATUS_CODE_ATTRIBUTE_NAME));
        return buildResponse(uri, status);
    }

    private String buildResponse(final Throwable errorCause) {
        return buildResponse(errorCause.getMessage());
    }

    private String buildResponse(final String uri, final HttpStatus status) {
        return buildResponse(status + " (path: '" + uri + "')");
    }

    private String buildResponse(final String message) {
        //todo 1 use thymeleaf  https://www.yawintutor.com/error-2100-circular-view-path-error/
        //todo or return json
        return "<html><body><h2>" + message + "</h2></body></html>";//todo here return json?
    }

    private Throwable getErrorCause(final HttpServletRequest request) {
        Throwable exception = (Throwable) request.getAttribute(DEFAULT_ERROR_ATTRIBUTE_NAME);
        if (exception == null) {
            exception = (Throwable) request.getAttribute(SERVLET_ERROR_ATTRIBUTE_NAME);
        }
        return exception;
    }

    private HttpStatus getStatus(final Integer status) {
        try {
            return HttpStatus.valueOf(status);
        } catch (final Exception ignore) {
            return HttpStatus.I_AM_A_TEAPOT;// :)
        }
    }

}
