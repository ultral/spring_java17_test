package pl.com.microservice.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@SpringBootApplication
@RestController
public class Backend {

    private final Environment environment;

    public Backend(final Environment environment) {
        this.environment = environment;
    }

    @SuppressWarnings("unused")// localhost:8080/profile
    @GetMapping("/profile")
    String profile() {
        StringBuilder profiles = new StringBuilder("Profile - ");
        for (String profileName : environment.getActiveProfiles()) {
            profiles.append("'").append(profileName).append("' ");
        }
        return profiles.toString();
    }

    @SuppressWarnings("unused")// localhost:8080/custom
    @GetMapping("/custom")
    String custom() {
        return "Custom backend response.";
    }

    @SuppressWarnings("all")// localhost:8080
    @GetMapping("/")
    Map<String, String> home(final HttpServletRequest request) {
        final var baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        var map = Map.of(
                "rest api persons", baseUrl + "/persons",
                "CRUD GUI thymeleaf", baseUrl + "/index",
                "custom response", baseUrl + "/custom",
                "application information", baseUrl + "/actuator",
                "application profile", baseUrl + "/profile",
                "error message", baseUrl + "/error",
                "It works!", baseUrl + "/"
                );
        return map;
    }

    public static void main(String[] args) {
        SpringApplication.run(Backend.class, args);
    }
}