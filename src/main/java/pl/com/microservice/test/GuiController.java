package pl.com.microservice.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import pl.com.microservice.test.model.Person;
import pl.com.microservice.test.model.PersonNotFoundException;
import pl.com.microservice.test.model.PersonRepository;
import pl.com.microservice.test.model.PositionEnum;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
@Controller
public class GuiController {

    private final PersonRepository personRepository;
    private final RestTemplate restTemplate;

    @Autowired
    public GuiController(final PersonRepository personRepository, @Value("${rest.api.root.uri}") final String rootUri) {
        this.personRepository = personRepository;
        final RestTemplateBuilder rtb = new RestTemplateBuilder();
        this.restTemplate = rtb.rootUri(rootUri).build();
    }

    @GetMapping("/index")
    public String showUserList(final Model model) {
        model.addAttribute("persons", personRepository.findAll());
        return "index";
    }

    @GetMapping("/indexRest")
    public String showUserListFromRest(final Model model) {
        var responseEntity = restTemplate.getForEntity("/persons", Person[].class);
        var restPersons = responseEntity.getBody();
        model.addAttribute("persons", restPersons);
        return "index";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") final int id) {
        personRepository.deleteById(id);
        return "redirect:/index";
    }

    @GetMapping("/deleteRest/{id}")
    public String deleteUserRest(@PathVariable("id") final int id) {
        restTemplate.delete("/persons/id/" + id);
        return "redirect:/index";
    }

    @GetMapping("/show_gui_signup")
    public String showSignUpForm(final Person person) {
        return "add-person";
    }

    @PostMapping("/postperson")
    public String addUser(final Person person, final BindingResult result, final Model model) {
        if (result.hasErrors()) {
            return "add-person";
        }

        personRepository.save(person);
        return "redirect:/index";
    }

    @ModelAttribute("allPositions")
    public List<PositionEnum> populatePositions() {
        return Arrays.asList(PositionEnum.values());
    }

    @GetMapping("/show_gui_edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, final Model model) {
        final var person = personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException(id));

        model.addAttribute("person", person);
        return "update-person";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") int id, final Person newPerson, final BindingResult result, final Model model) {
        if (result.hasErrors()) {
            newPerson.setId(id);
            return "update-person";
        }
        var person = personRepository.findById(id).orElseThrow(() -> new PersonNotFoundException(id));
        person.setName(newPerson.getName());
        person.setSalary(newPerson.getSalary());
        person.setPosition(newPerson.getPosition());
        person.setBirthDate(newPerson.getBirthDate());
        personRepository.save(person);
        return "redirect:/index";
    }
}
