package pl.com.microservice.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import pl.com.microservice.test.model.Initializer;
import pl.com.microservice.test.model.Person;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonControllerTest {

    private final TestRestTemplate restTemplate;

    @Autowired
    public PersonControllerTest(final TestRestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Test
    public void testPersons() {
        //see: https://www.baeldung.com/spring-rest-template-list
        var responseEntity = this.restTemplate.getForEntity("/persons", Person[].class);
        var restPersons = responseEntity.getBody();
        assertThat(restPersons != null).isTrue();
        var restPersonsList = List.of(restPersons);
        var initPersonsList = Initializer.initPersonList();
        assertThat(restPersons.length).isEqualTo(initPersonsList.size());
        assertThat(restPersonsList.containsAll(initPersonsList)).isTrue();
        assertThat(initPersonsList.containsAll(restPersonsList)).isTrue();

        restPersonsList.forEach(it -> this.restTemplate.delete("/persons/id/"+it.getId()));
        responseEntity = this.restTemplate.getForEntity("/persons", Person[].class);
        restPersons = responseEntity.getBody();
        assertThat(restPersons != null).isTrue();
        assertThat(restPersons.length).isZero();
    }

}
