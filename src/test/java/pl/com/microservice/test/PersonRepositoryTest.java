package pl.com.microservice.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import pl.com.microservice.test.model.PersonBuilder;
import pl.com.microservice.test.model.PersonRepository;

import java.util.Date;

import static java.math.BigDecimal.ONE;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.com.microservice.test.model.PositionEnum.worker;

@ActiveProfiles("dev")
@SpringBootTest
public class PersonRepositoryTest {

    private final PersonRepository personRepository;

    @Autowired
    public PersonRepositoryTest(final PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @AfterEach
    public void clear(){
        personRepository.deleteAll();
    }
    
    @Test
    public void findPerson() {
        var person = personRepository.save(PersonBuilder.aPerson().withName("Marek").withBithDate(new Date()).withSalary(ONE).withPosition(worker).build());
        var repoPerson = personRepository.findById(person.getId()).orElseThrow();
        assertThat(person.equals(repoPerson)).isTrue();
    }

    @Test
    public void findSamePersons() {
        personRepository.save(PersonBuilder.aPerson().withName("Marek").withBithDate(new Date()).withSalary(ONE).withPosition(worker).build());
        personRepository.save(PersonBuilder.aPerson().withName("Marek").withBithDate(new Date()).withSalary(ONE).withPosition(worker).build());
        assertThat(personRepository.count() == 2).isTrue();
    }

    @Test
    public void findPersons() {
        personRepository.save(PersonBuilder.aPerson().withName("Marek").withBithDate(new Date()).withSalary(ONE).withPosition(worker).build());
        personRepository.save(PersonBuilder.aPerson().withName("Marek2").withBithDate(new Date()).withSalary(ONE).withPosition(worker).build());
        assertThat(personRepository.count() == 2).isTrue();
    }

    @Test
    public void findPersonsAutoId() {
        var person = personRepository.save(PersonBuilder.aPerson().withName("Marek").withBithDate(new Date()).withSalary(ONE).withPosition(worker).build());
        var newName = "Marek2";
        person.setName(newName);
        personRepository.save(person);
        assertThat(personRepository.findById(person.getId()).orElseThrow().getName().equals(newName)).isTrue();
    }
}
