package pl.com.microservice.test;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BackendTest {

    private final TestRestTemplate restTemplate;

    @Autowired
    public BackendTest(final TestRestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Test
    public void staticResponses() {
        var body = this.restTemplate.getForObject("/custom", String.class);
        assertThat(body).isEqualTo("Custom backend response.");
        body = this.restTemplate.getForObject("/", String.class);
        assertThat(body).contains("It works!");
        body = this.restTemplate.getForObject("/profile", String.class);
        assertThat(body.startsWith("Profile - ")).isTrue();
    }

    @Test
    public void errorResponses() {
        var body = this.restTemplate.getForObject("/customs", String.class);
        assertThat(body).contains("404");
        int id = -666;
        body = this.restTemplate.getForObject("/persons/id/"+id, String.class);
        assertThat(body).contains(String.valueOf(id));
        assertThat(body).contains("not found");
        body = this.restTemplate.getForObject("/persons/id/123123123123123", String.class);
        assertThat(body).contains("NumberFormatException");
    }
}
