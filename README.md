### Java Spring microservice template project 

This project is based on a [Project Template](https://gitlab.com/ultral/spring_java17_test).

### Tools needed to download and compile project

Git distributed version control system [download link](https://git-scm.com/download).

The Java Platform, Standard Edition 11 Development Kit [download link](https://www.oracle.com/java/technologies/downloads/#java11).

Apache Maven [download link](https://maven.apache.org/download.cgi).

### Building

dev: clean install or clean install -Pdev 

prod: clean install -Pprod

### Running

from console: java -jar microservice-test-0.0.1-SNAPSHOT.jar
from web browser: http://localhost:8080 [link](http://localhost:8080)
actuator: http://localhost:8080/actuator [link](http://localhost:8080/actuator)
persons rest api: http://localhost:8080/persons [link](http://localhost:8080/persons)